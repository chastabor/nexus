FROM rust:1.35.0 AS build
WORKDIR /usr/src

# Download the target for static linking.
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -y install musl-tools \
  && rustup target add x86_64-unknown-linux-musl

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN USER=root cargo new nexus
WORKDIR /usr/src/nexus
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application and health checker.
COPY src ./src
RUN cargo test \
  && cargo install --target x86_64-unknown-linux-musl --path . 
  #&& cd health \
  #&& cargo install --target x86_64-unknown-linux-musl --path .

# Copy the statically-linked binary into a scratch container.
FROM scratch
COPY --from=build /usr/local/cargo/bin/nexus .
USER 1000
#HEALTHCHECK CMD ["/health"]
CMD ["./nexus"]
