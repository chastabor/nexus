//use crypto::blake2b::Blake2b;

#[allow(dead_code, unused_imports)]
#[path = "page_generated.rs"]
mod page_generated;
pub use page_generated::{
    Site, SiteArgs,
    Template, TemplateArgs,
    Page, PageArgs,
    Contents,
    Branch, BranchArgs, get_root_as_branch,
};
pub use flatbuffers::{FlatBufferBuilder, WIPOffset};

// Content build helpers
type Content = flatbuffers::WIPOffset<flatbuffers::UnionWIPOffset>;
pub enum TSM<'args: 'mut_bldr, 'mut_bldr> {
    T {
        data: Option<&'args str>,
        partial: bool,
        pages: Option<&'mut_bldr [WIPOffset<Page<'args>>]>,
    },
    S {
        branch: &'args str,
    },
}

pub fn build_content<'args: 'mut_bldr, 'mut_bldr>(
        bldr: &'mut_bldr mut FlatBufferBuilder<'args>,
        tsm: TSM<'args, 'mut_bldr>,
    ) -> (Contents, Content) {
    match tsm {
        TSM::S{branch: b} => {
            let site_args = SiteArgs{
                branch: Some(bldr.create_string(b)),
            };
            (Contents::Site, Site::create(bldr, &site_args).as_union_value())
        },
        TSM::T{data: d, partial: p, pages: pgs} => {
            let template_args = TemplateArgs{
                data: if let Some(data) = d {Some(bldr.create_string(data))} else {None},
                partial: p,
                pages: if let Some(pages) = pgs {Some(bldr.create_vector(pages))} else {None},
            };
            (Contents::Template, Template::create(bldr, &template_args).as_union_value())
        },
    }
}

pub fn build_page<'args: 'mut_bldr, 'mut_bldr>(
        bldr: &'mut_bldr mut FlatBufferBuilder<'args>,
        uuid: &'args [u8],
        path: Option<&'args str>,
        tsm: TSM<'args, 'mut_bldr>,
    ) -> WIPOffset<Page<'args>> {
    let (content_type, content) = build_content(bldr, tsm);
    let page_args = PageArgs{
        uuid: Some(bldr.create_vector(&uuid)),
        path: if let Some(path) = path {Some(bldr.create_string(path))} else {None},
        content_type: content_type,
        content: Some(content),
    };
    Page::create(bldr, &page_args)
}

pub fn build_branch<'args: 'mut_bldr, 'mut_bldr>(
        bldr: &'mut_bldr mut FlatBufferBuilder<'args>,
        comment: Option<&'args str>,
        source: Option<&'args str>,
        page: Option<WIPOffset<Page<'args>>>,
    ) -> WIPOffset<Branch<'args>> {
    let branch_args = BranchArgs{
        comment: if let Some(comment) = comment {Some(bldr.create_string(comment))} else {None},
        source: if let Some(source) = source {Some(bldr.create_string(source))} else {None},
        page: page,
    };
    Branch::create(bldr, &branch_args)
}
#[derive(Debug, PartialEq)]
pub struct PageInfo<'a> {
    uuid: &'a [u8],
    path: Option<&'a str>,
    content: Option<PageInfoContent<'a>>,
}

// If pages field is NOT None it may have
// multiple types of CRUD.
//   1) Insertions (Create)
//   2) Reordering (Update)
//   3) Path name changes (Update)
//   4) Deletions (Delete)
//   5) A branch to changes downline (Read)
// if partial field is true then Template
// does NOT contain all the children as its
// pages field is acting only as path to
// changes that may be found further down
// that part of the branch.
#[derive(Debug, PartialEq)]
pub enum PageInfoContent<'a> {
    Template {
        data: Option<&'a str>,
        partial: bool,
        pages: Option<Vec<PageInfo<'a>>>,
    },
    Site(&'a str),
}

pub fn get_children<'a>(template: Template<'a>) -> Option<Vec<PageInfo>> {
    if let Some(pages) = template.pages() {
        let mut list = Vec::new();
        let mut i = 0;
        while i < pages.len() {
            list.push(PageInfo {
                uuid: pages.get(i).uuid(),
                path: pages.get(i).path(),
                content: None,
            });
            i += 1;
        }
        Some(list)
    } else {
        None
    }
}

#[derive(Debug, PartialEq)]
pub enum PageId<'a> {
    Path(&'a str),
    Uuid(&'a [u8]),
}

fn get_child<'a, 'b>(template: Template<'a>, id: &'b PageId) -> Option<Page<'a>> {
    if let Some(pages) = template.pages() {
        let mut i = 0;
        while i < pages.len() {
            let page = pages.get(i);
            match id {
                PageId::Path(p) => if page.path() == Some(*p) {
                    return Some(page);
                },
                PageId::Uuid(p) => if page.uuid() == *p {
                    return Some(page)
                },
            };
            i += 1;
        }
    }
    None
}

pub fn find_page_info<'a, 'b>(buf: &'a [u8], ids: &'b [PageId]) -> (Option<PageInfo<'a>>, &'b [PageId<'b>]) {
    let r = get_root_as_branch(buf);
    let mut level = None;
    let mut page = r.page();
    for (i, id) in ids.iter().enumerate() {
        if let Some(pg) = page {
            match pg.content_type() {
                Contents::Site => {
                    let site = pg.content_as_site().unwrap();
                    return (Some(PageInfo{
                        uuid: pg.uuid(),
                        path: pg.path(),
                        content: Some(PageInfoContent::Site(site.branch())),
                    }), &ids[i..]);
                },
                Contents::Template => {
                    let template = pg.content_as_template().unwrap();
                    page = get_child(template, id);
                    if page.is_none() {
                        return (Some(PageInfo{
                            uuid: pg.uuid(),
                            path: pg.path(),
                            content: Some(PageInfoContent::Template{
                                data: template.data(),
                                partial: template.partial(),
                                pages: get_children(template),
                            }),
                        }), &ids[i..]);
                    }
                },
                Contents::NONE => {
                    return (Some(PageInfo{
                        uuid: pg.uuid(),
                        path: pg.path(),
                        content: None,
                    }), &ids[i..]);
                },
            }
            if page.is_some() {
                // match found, so consume id
                level = Some(i+1);
            } else {
                // No match found, so do not consume id
                level = Some(i);
            }
        } else {
            return (None, &ids[i..]);
        }
    }
    let level = if let Some(i) = level {i} else {0}; 
    if let Some(pg) = page {
        (Some(PageInfo{
            uuid: pg.uuid(),
            path: pg.path(),
            content: match pg.content_type() {
                Contents::Site => {
                    let site = pg.content_as_site().unwrap();
                    Some(PageInfoContent::Site(site.branch()))
                },
                Contents::Template => {
                    let template = pg.content_as_template().unwrap();
                    Some(PageInfoContent::Template {
                        data: template.data(),
                        partial: template.partial(),
                        pages: get_children(template),
                    })
                },
                Contents::NONE => None,
            },
        }), &ids[level..])
    } else {
        (None, &ids[level..])
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const ROOT_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const ROOT_DATA: &str = "abc...123";
    const ROOT_COMMENT: &str = "initialize";
    const ROOT_SOURCE: &str = "www.example.com/ROOT/PUBLISHED/1";
    const PAGE1_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
    const PAGE1_PATH: &str = "page1";
    const PAGE1_DATA: &str = "bcd...234";
    const PAGE2_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2];
    const PAGE2_PATH: &str = "page2";
    const PAGE2_BRANCH: &str = "www.example.com/site1/PUBLISHED";
    const PAGE3_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3];
    const PAGE3_PATH: &str = "page3";
    const PAGE4_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4];
    const PAGE4_PATH: &str = "page4";
    const PAGE4_DATA: &str = "def...456";
    const PAGE4_PATH_DELTA1: &str = "page4_d1";
    const PAGE4_DATA_DELTA1: &str = "def...456_d1";
    const PAGE5_UUID: &[u8; 16] = &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5];
    const PAGE5_PATH: &str = "page5";
    const PAGE5_BRANCH: &str = "www.example.com/site2/PUBLISHED";
    const PAGE5_BRANCH_DELTA1: &str = "www.example.com/site2_d1/PUBLISHED";

    // root level | level1 | level2
    //            |        |
    // T(ROOT)    | T(Pg1) | T(Pg4)
    //            |        | S(Pg5)
    //            | S(Pg2)
    // 
    fn build_fixture(mut builder: &mut FlatBufferBuilder) {
        let mut pages_layer2 = Vec::new();
        pages_layer2.push(build_page(
            &mut builder,
            PAGE4_UUID,
            Some(PAGE4_PATH),
            TSM::T {
                data: Some(PAGE4_DATA),
                partial: false,
                pages: Some(&[]),
            },
        ));
        pages_layer2.push(build_page(
            &mut builder,
            PAGE5_UUID,
            Some(PAGE5_PATH),
            TSM::S {
                branch: PAGE5_BRANCH,
            },
        ));
        let mut pages_layer1 = Vec::new();
        pages_layer1.push(build_page(
            &mut builder,
            PAGE1_UUID,
            Some(PAGE1_PATH),
            TSM::T {
                data: Some(PAGE1_DATA),
                partial: false,
                pages: Some(&pages_layer2),
            },
        ));
        pages_layer1.push(build_page(
            &mut builder,
            PAGE2_UUID,
            Some(PAGE2_PATH),
            TSM::S {
                branch: PAGE2_BRANCH,
            },
        ));
        let root_page = build_page(
            &mut builder,
            ROOT_UUID,
            None,
            TSM::T {
                data: Some(ROOT_DATA),
                partial: false,
                pages: Some(&pages_layer1),
            },
        );
        let branch = build_branch(
            &mut builder,
            Some(ROOT_COMMENT),
            Some(ROOT_SOURCE),
            Some(root_page),
        );
        builder.finish(branch, None)
    }

    // root level | level1 | level2
    //            |        |
    // T(ROOT)    | T(Pg1) | T(Pg4)
    //            |        | S(Pg5)
    //
    // ROOT, Pg1 and Pg2 have no changes.
    // Pg4 has content data and path change.
    // Pg5 has branch change.
    fn build_fixture_delta(mut builder: &mut FlatBufferBuilder) {
        let mut pages_layer2 = Vec::new();
        pages_layer2.push(build_page(
            &mut builder,
            PAGE4_UUID,
            Some(PAGE4_PATH_DELTA1),
            TSM::T {
                data: Some(PAGE4_DATA_DELTA1),
                partial: false,
                pages: None,
            },
        ));
        pages_layer2.push(build_page(
            &mut builder,
            PAGE5_UUID,
            Some(PAGE5_PATH),
            TSM::S {
                branch: PAGE5_BRANCH_DELTA1,
            },
        ));
        let mut pages_layer1 = Vec::new();
        pages_layer1.push(build_page(
            &mut builder,
            PAGE1_UUID,
            None,
            TSM::T {
                data: None,
                partial: true,
                pages: Some(&pages_layer2),
            },
        ));
        let root_page = build_page(
            &mut builder,
            ROOT_UUID,
            None,
            TSM::T {
                data: None,
                partial: true,
                pages: Some(&pages_layer1),
            },
        );
        let branch = build_branch(
            &mut builder,
            None, // Comments are only for commits to REVIEW or PUBLISHED branches
            Some(ROOT_SOURCE),
            Some(root_page),
        );
        builder.finish(branch, None)
    }

    #[test]
    fn test_serialize() {
        let mut builder = FlatBufferBuilder::new();
        build_fixture(&mut builder);
        let buf = builder.finished_data();
        let r = get_root_as_branch(buf);
        assert_eq!(r.source(), Some(ROOT_SOURCE));
        assert_eq!(r.comment(), Some(ROOT_COMMENT));
        assert_eq!(r.page().unwrap().content_as_template().unwrap().data(), Some(ROOT_DATA));
        assert!(r.page().unwrap().content_as_template().unwrap().pages().is_some());
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().len(), 2);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).uuid(), PAGE1_UUID);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).path(), Some(PAGE1_PATH));
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_type(), Contents::Template);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().data(), Some(PAGE1_DATA));
        assert!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().is_some());
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).uuid(), PAGE4_UUID);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(1).uuid(), PAGE2_UUID);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(1).path(), Some(PAGE2_PATH));
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(1).content_type(), Contents::Site);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(1).content_as_site().unwrap().branch(), PAGE2_BRANCH);
    }

    #[test]
    fn test_serialize_delta() {
        let mut builder = FlatBufferBuilder::new();
        build_fixture_delta(&mut builder);
        let buf = builder.finished_data();
        let r = get_root_as_branch(buf);
        assert_eq!(r.source(), Some(ROOT_SOURCE));
        assert_eq!(r.comment(), None);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().data(), None);
        assert!(r.page().unwrap().content_as_template().unwrap().pages().is_some());
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().len(), 1);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).uuid(), PAGE1_UUID);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).path(), None);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_type(), Contents::Template);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().data(), None);
        assert!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().is_some());
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).uuid(), PAGE4_UUID);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).path(), Some(PAGE4_PATH_DELTA1));
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).content_type(), Contents::Template);
        assert_eq!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().data(), Some(PAGE4_DATA_DELTA1));
        assert!(r.page().unwrap().content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().unwrap().get(0).content_as_template().unwrap().pages().is_none());
    }

    #[test]
    fn test_find_by_uuids_or_paths() {
        let mut builder = FlatBufferBuilder::new();
        build_fixture(&mut builder);
        let buf = builder.finished_data();

        // []                    -> (T(ROOT),   [])
        let mut page = find_page_info(buf, &[]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: ROOT_UUID,
            path: None,
            content: Some(PageInfoContent::Template{
                data: Some(ROOT_DATA),
                partial: false,
                pages: Some(vec![
                    PageInfo{
                        uuid: PAGE1_UUID,
                        path: Some(PAGE1_PATH),
                        content: None,
                    },
                    PageInfo{
                        uuid: PAGE2_UUID,
                        path: Some(PAGE2_PATH),
                        content: None,
                    },
                ]),
            }),
        }));
        assert_eq!(page.1, &[]);

        // [Page1]               -> (T(Page1),  [])
        page = find_page_info(buf, &[PageId::Uuid(PAGE1_UUID)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE1_UUID,
            path: Some(PAGE1_PATH),
            content: Some(PageInfoContent::Template{
                data: Some(PAGE1_DATA),
                partial: false,
                pages: Some(vec![
                    PageInfo{
                        uuid: PAGE4_UUID,
                        path: Some(PAGE4_PATH),
                        content: None,
                    },
                    PageInfo{
                        uuid: PAGE5_UUID,
                        path: Some(PAGE5_PATH),
                        content: None,
                    },
                ]),
            }),
        }));
        assert_eq!(page.1, &[]);

        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE1_UUID,
            path: Some(PAGE1_PATH),
            content: Some(PageInfoContent::Template{
                data: Some(PAGE1_DATA),
                partial: false,
                pages: Some(vec![
                    PageInfo{
                        uuid: PAGE4_UUID,
                        path: Some(PAGE4_PATH),
                        content: None,
                    },
                    PageInfo{
                        uuid: PAGE5_UUID,
                        path: Some(PAGE5_PATH),
                        content: None,
                    },
                ]),
            })
        }));
        assert_eq!(page.1, &[]);

        // [Page1, Page4]        -> (T(Page4), [])
        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH), PageId::Path(PAGE4_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE4_UUID,
            path: Some(PAGE4_PATH),
            content: Some(PageInfoContent::Template{
                data: Some(PAGE4_DATA),
                partial: false,
                pages: Some(vec![]),
            }),
        }));
        assert_eq!(page.1, &[]);

        // [Page1, Page5]        -> (S(Page5), [])
        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH), PageId::Path(PAGE5_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE5_UUID,
            path: Some(PAGE5_PATH),
            content: Some(PageInfoContent::Site(PAGE5_BRANCH)),
        }));
        assert_eq!(page.1, &[]);

        // Paths that continue past a Template return None along with page not found
        // [Page1, Page3]        -> (None,      [Page3])
        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH), PageId::Path(PAGE3_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE1_UUID,
            path: Some(PAGE1_PATH),
            content: Some(PageInfoContent::Template{
                data: Some(PAGE1_DATA),
                partial: false,
                pages: Some(vec![
                    PageInfo{
                        uuid: PAGE4_UUID,
                        path: Some(PAGE4_PATH),
                        content: None,
                    },
                    PageInfo{
                        uuid: PAGE5_UUID,
                        path: Some(PAGE5_PATH),
                        content: None,
                    },
                ]),
            }),
        }));
        assert_eq!(page.1, &[PageId::Path(PAGE3_PATH)]);

        // Paths that continue past a Template return last Template along with
        // the list of pages not found
        // [Page1, Page4, Page3] -> (T(Page4), [Page3])
        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH), PageId::Path(PAGE4_PATH), PageId::Path(PAGE3_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE4_UUID,
            path: Some(PAGE4_PATH),
            content: Some(PageInfoContent::Template{
                data: Some(PAGE4_DATA),
                partial: false,
                pages: Some(vec![]),
            }),
        }));
        assert_eq!(page.1, &[PageId::Path(PAGE3_PATH)]);

        // Paths that continue past a Site return Site Info along with
        // the list of pages not found
        // [Page1, Page5, Page3] -> (SM(Page5), [Page3])
        page = find_page_info(buf, &[PageId::Path(PAGE1_PATH), PageId::Path(PAGE5_PATH), PageId::Path(PAGE3_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE5_UUID,
            path: Some(PAGE5_PATH),
            content: Some(PageInfoContent::Site(PAGE5_BRANCH)),
        }));
        assert_eq!(page.1, &[PageId::Path(PAGE3_PATH)]);

        // [Page2]               -> (S(Page2),  [])
        page = find_page_info(buf, &[PageId::Path(PAGE2_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE2_UUID,
            path: Some(PAGE2_PATH),
            content: Some(PageInfoContent::Site(PAGE2_BRANCH)),
        }));
        assert_eq!(page.1, &[]);

        // [Page2, Page3]        -> (S(Page2),  [Page3])
        page = find_page_info(buf, &[PageId::Path(PAGE2_PATH), PageId::Path(PAGE3_PATH)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: PAGE2_UUID,
            path: Some(PAGE2_PATH),
            content: Some(PageInfoContent::Site(PAGE2_BRANCH)),
        }));
        assert_eq!(page.1, &[PageId::Path(PAGE3_PATH)]);

        // [Page3]               -> (None,      [Page3])
        page = find_page_info(buf, &[PageId::Uuid(PAGE3_UUID)]);
        assert_eq!(page.0, Some(PageInfo{
            uuid: ROOT_UUID,
            path: None,
            content: Some(PageInfoContent::Template{
                data: Some(ROOT_DATA),
                partial: false,
                pages: Some(vec![
                    PageInfo{
                        uuid: PAGE1_UUID,
                        path: Some(PAGE1_PATH),
                        content: None,
                    },
                    PageInfo{
                        uuid: PAGE2_UUID,
                        path: Some(PAGE2_PATH),
                        content: None,
                    },
                ]),
            }),
        }));
        assert_eq!(page.1, &[PageId::Uuid(PAGE3_UUID)]);
    }

//    fn test_serialize_from_page_info() {}
//    fn test_serialize_from_page_info_delta() {}
}
