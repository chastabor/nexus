# nexus

Version Controlled Content Management System.  Editors manage their branch and push changes to a published branch. Rust application with FlatBuffers for schema management/data sharing and with Minio/S3 as a backend object storage.

Ubuntu: Install FlatBuffers software for rust code generation:
`sudo snap install --edge flatbuffers`

Run flatbuffers with fbs schema files to generate rust source code:
`flatc --rust -o src/schemas flatbuffers/page.fbs`